import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core';
import { TreeNode as Tree } from 'angular2-tree-component';
import {Location} from '@angular/common';
import { MaterialModule } from '@angular/material';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ButtonModule} from 'primeng/primeng';



//services
import { ActionService } from '../../services/ActionService';

//model
import {ActionType} from '../../model/ui/ActionType';
import {IActions} from '../../model/ui/IActions';
import { Bundle } from 'app/model/ui/Bundle';
import {DataType} from 'app/model/ui/DataType';
import { ButtonType } from "app/model/ui/ButtonType";

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})


export class MainContentComponent implements IActions, OnInit, OnDestroy {
  
   

  @ViewChild('mySidenav') sidenav;
  @ViewChild('containerinside') maincontaner;
  @ViewChild('mat-sidenav-backdrop') backdrop;

  //top bar buttons states false/disable by default
  insertButton:boolean = false;
  createButton:boolean = false;
  updateButton:boolean = false;
  deleteButton:boolean = false;

  isOpen: boolean = false;
  private title: string = 'title';
  selectedRouteLink: string;
  navDisplay:string = 'none';

  treeArray:any[] = [];

  constructor(private actionService: ActionService,private location: Location) { }

  ngOnInit() {
    this.actionService.register(this.constructor.name,this);

    for(let i =0; i < 50; i++){
      this.treeArray[i] = {display:'none'};
    }
  }


  ngOnDestroy(): void {
    this.actionService.unRegister(this.constructor.name);
  }


  expandMenu(num){
    if(this.treeArray[num].display == 'none') {
      this.treeArray[num].display = 'table';
    } else {
      this.treeArray[num].display = 'none';
    }
  }

  getContainerMargin() {
    if(!this.isOpen){
      return '50px';
    } else {
      return '225px';
    }
  }

  getNavStyle(){
    if(!this.isOpen){
      return '50px';
    } else {
      return '225px';
    }
  }

  sidenavclick(){
    if(this.isOpen){
       this.isOpen = false;
       this.navDisplay = 'none';
    } else {
      this.isOpen = true;
      this.navDisplay = 'table';
    }
  }

  closenav(){
    this.isOpen = false;
  }

  goBack() {
    //add check to not leave domain
    this.location.back();
  }



  //action button clicks
  insertClick() {
    this.actionService.notifyAll(ActionType.INSERT);
  }

  updateClick(){
    this.actionService.notifyAll(ActionType.UPDATE);
  }

  deleteClick() {
    this.actionService.notifyAll(ActionType.DELETE);
  }
  //end action button clicks


  //---->Start interface methods
  uiAction(button: ButtonType, state: boolean) {
      this.initButton(button,state);
  }

  receive(bundle: Bundle) {
  }

  insert() {
  }

  create() {
  }

  delete() {
  }

  update() {

  }
  //---End interface methods
 


  private initButton(type:ButtonType,state:boolean){
      if(type == ButtonType.CREATE) {
          this.setCreateButton(state);
      } else if(type == ButtonType.DELETE){
          this.setDeleteButton(state);
      } else if(type == ButtonType.INSERT) {
          this.setInsertButton(state);
      } else if(type == ButtonType.UPDATE) {
          this.setUpdateButton(state);
      } else if(type == ButtonType.ALL) {
          this.setCreateButton(state);
          this.setDeleteButton(state);
          this.setInsertButton(state);
          this.setUpdateButton(state);
      }
  }
  

  private setCreateButton(state:boolean){
    this.createButton = state;
  }

  private setDeleteButton(state:boolean) {
    this.deleteButton = state;
  }

  private setInsertButton(state:boolean) {
    this.insertButton = state;
  }

  private setUpdateButton(state:boolean) {
    this.updateButton = state;
  }


  
}


