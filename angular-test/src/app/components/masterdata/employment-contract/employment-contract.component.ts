import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//models
import {URLMASTERDATA} from '../../../model/Url';
import {EmploymentContract} from '../../../model/masterdata/common/EmploymentContract';

@Component({
  selector: 'app-employment-contract',
  templateUrl: './employment-contract.component.html',
  styleUrls: ['./employment-contract.component.css']
})

export class EmploymentContractComponent implements OnInit {

  employesContracts:EmploymentContract[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<EmploymentContract>(URLMASTERDATA['employmentcontracts']).subscribe(
         (res)=> {
            this.employesContracts = res;
         },
         (err)=> {

         }
      );
  }

}
