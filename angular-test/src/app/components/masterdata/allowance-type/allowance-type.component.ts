import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//models
import {URLMASTERDATA} from '../../../model/Url';
import {AllowanceType} from '../../../model/masterdata/common/AllowanceType';

@Component({
  selector: 'app-allowance-type',
  templateUrl: './allowance-type.component.html',
  styleUrls: ['./allowance-type.component.css']
})
export class AllowanceTypeComponent implements OnInit {

  allowanceTypes:AllowanceType[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<AllowanceType>(URLMASTERDATA['allowancetypes']).subscribe(
         (res)=> {
            this.allowanceTypes = res;
         },
         (err)=> {

         }
      );
  }


}
