import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxOfficeComponent } from './taxoffice.component';

describe('EforiesComponent', () => {
  let component: TaxOfficeComponent;
  let fixture: ComponentFixture<TaxOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
