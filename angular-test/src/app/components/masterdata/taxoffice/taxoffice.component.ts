import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//model imports
import {TaxOffice} from '../../../model/masterdata/common/TaxOffice';
import {URLMASTERDATA} from '../../../model/Url';

@Component({
  selector: 'app-efories',
  templateUrl: './taxoffice.component.html',
  styleUrls: ['./taxoffice.component.css']
})

export class TaxOfficeComponent implements OnInit {

  taxOffices:TaxOffice[] = [];
  selectedTaxOffice:TaxOffice = new TaxOffice();

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getAsync<TaxOffice>(URLMASTERDATA['taxoffices']).subscribe(
      (res)=> {
          this.taxOffices = res;
      },
      (err)=> {
        console.log(err);
      }
    );
  }

}
