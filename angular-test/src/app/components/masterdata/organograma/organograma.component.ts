import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

import {DataTableModule,SharedModule,ButtonModule,DialogModule,TreeNode,TreeModule} from 'primeng/primeng';
import {HttpService} from '../../../services/HttpService';
import {MyTreeNode} from '../../../model/MyTreeNode';
import {URLMASTERDATA} from '../../../model/Url';


@Component({
  selector: 'app-organograma',
  templateUrl: './organograma.component.html',
  styleUrls: ['./organograma.component.css']
})


export class OrganogramaComponent implements OnInit {

  nodes: MyTreeNode[];
  selectedNode:MyTreeNode = new MyTreeNode;

  constructor(private httpService: HttpService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    this.httpService.getAsync<MyTreeNode>(URLMASTERDATA['unitroot']).subscribe(
        (res)=> {
            console.log(res);
            this.nodes = res;

            for(let i =0; i< this.nodes.length; i++) {
              this.nodes[i].label = this.nodes[i].description;
              this.nodes[i].expandedIcon = 'fa-folder-open';
              this.nodes[i].collapsedIcon = 'fa-folder';
            }
            
        },
        (err)=> {
          console.log(err);
        }
    );
  }


  nodeSelect(event) {
     console.log('selectednode:'+this.selectedNode.description+',ID:'+this.selectedNode.id);
     if(event.node.expanded){
       this.router.navigate(['/unitroot/unit/'+event.node.id]);
     } else {
      
       this.httpService.getAsync<MyTreeNode>('http://localhost:8080/payroll/api/masterdata/units/'+this.selectedNode.id+'/units/').subscribe
        (
            (res)=> {
                if(res){
                 
                  if(res.length > 0) {
                    this.selectedNode = res[0];
                    for(let i =0; i < res.length; i++){
                        res[i].label = res[i].description;
                        res[i].expandedIcon = 'fa-folder-open';
                        res[i].collapsedIcon = 'fa-folder';
                    } 

                    this.selectedNode = res[0];

                    event.node.expanded = true;
                    event.node.children = res;
                    
                  } else if(res.length == 0) {
                    //event.node = res;
                  }

                  console.log('res.length'+res.length);
                  console.log('eventID:'+event.node.id);
                  this.router.navigate(['/unitroot/unit/'+event.node.id]);
                  
                }
            },
            (err)=> {
              console.log(err);
            }
        );
     }

     
  }

  nodeUnselect(event) {
    event.node.expanded = false;
  }


}
