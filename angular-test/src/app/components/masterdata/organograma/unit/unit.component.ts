import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params  }   from '@angular/router';
import { Location }                 from '@angular/common';

import 'rxjs/add/operator/switchMap';

import {URLMASTERDATA} from '../../../../model/Url';

//primeng import
import {DataTableModule,SharedModule,ButtonModule,DialogModule,InputTextModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../../services/HttpService';

//models
import {OrganizationUnit} from '../../../../model/masterdata/common/OrganizationUnit';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.css']
})


export class UnitComponent implements OnInit {

  units: OrganizationUnit[];

  constructor(private httpService: HttpService,private route: ActivatedRoute,private location: Location) { }

  ngOnInit() {
       let id =0;
      this.route.params
      .switchMap((params: Params) => this.httpService.getAsync<OrganizationUnit>(URLMASTERDATA['unit']+(id = params['id'])+'/units/')).subscribe(
        (res)=> {
            this.units = res;       
        },
        (err)=>{
          console.log(err);
        }
      )
  }

}
