import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//model import
import {Sector} from '../../../model/masterdata/common/Sector';
import {Speciality} from '../../../model/masterdata/common/Speciality';
import {URLMASTERDATA} from '../../../model/Url';

//service import
import {HttpService} from '../../../services/HttpService';

@Component({
  selector: 'app-eidikotita',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.css']
})

export class SectorComponent implements OnInit {

  sectors:Sector[];
  selectedSector:Sector;
  selectedSpeciality:Speciality;
  specialities:Speciality[];
  stacked:boolean = false;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<Sector>(URLMASTERDATA['sectors']).subscribe(
        (res)=>{
            this.sectors = res;
        },
        (err)=> {
          console.log(err);
        }
      );
  }


  onRowSelectSector(event) {
      this.selectedSector = event.data;
      this.httpService.getAsync<Speciality>(URLMASTERDATA['sectors']+this.selectedSector.id+'/specialities/').subscribe(
          (res)=> {
            /*
              let temp = new Speciality();
              temp.description = this.selectedSector.description;
              temp.usrcode = this.selectedSector.usrcode;
              temp.id = this.selectedSector.id;
              temp.type = this.selectedSector.type;
              temp.version = this.selectedSector.version;
              res.push(temp);
              */

              this.specialities = res;
              console.log(res);
          },
          (err)=> {
            console.log(err);
          }
      );
  }


  onRowSelectSpeciality(event) {
      this.selectedSpeciality = event.data;
  }

  toggle() {
    this.stacked = !this.stacked;
  }

}
