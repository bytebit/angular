import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionGroupComponent } from './deduction-group.component';

describe('DeductionGroupComponent', () => {
  let component: DeductionGroupComponent;
  let fixture: ComponentFixture<DeductionGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
