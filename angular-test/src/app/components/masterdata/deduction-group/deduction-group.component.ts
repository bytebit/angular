import { Component, OnInit } from '@angular/core';
import {DataTableModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//models
import {URLMASTERDATA} from '../../../model/Url';
import {InsuranceDeductionGroup} from '../../../model/masterdata/deduction/InsuranceDeductionGroup';
import {InsuranceDeductionGroupItem} from '../../../model/masterdata/deduction/InsuranceDeductionGroupItem';


@Component({
  selector: 'app-deduction-group',
  templateUrl: './deduction-group.component.html',
  styleUrls: ['./deduction-group.component.css']
})
export class DeductionGroupComponent implements OnInit {

  insurDeductionGroups:InsuranceDeductionGroup[] = [];
  insurDeductionGroupsItems:InsuranceDeductionGroupItem[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getAsync<InsuranceDeductionGroup>(URLMASTERDATA['deductiongroups']).subscribe(
        (res)=> {
            this.insurDeductionGroups = res;
        },
        (err) => {

        }
    );
  }


  onRowSelectGroup(event) {
     this.httpService.getAsync<InsuranceDeductionGroupItem>(URLMASTERDATA['deductiongroups']+event.data.id+'/items/').subscribe(
          (res)=> {
              this.insurDeductionGroupsItems = res;
          },
          (err)=> {

          }
     );
  }


}
