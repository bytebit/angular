import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//models
import {URLMASTERDATA} from '../../../model/Url';
import {OrganizationArea} from '../../../model/masterdata/common/OrganizationArea';



@Component({
  selector: 'app-perioxesforeon',
  templateUrl: './organizationarea.component.html',
  styleUrls: ['./organizationarea.component.css']
})

export class OrganizationAreaComponent implements OnInit {

  areas:OrganizationArea[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<OrganizationArea>(URLMASTERDATA['organizationareas']).subscribe(
        (res)=> {
            this.areas = res;
        },
        (err)=> {
          console.log(err);
        }
      );
  }

}
