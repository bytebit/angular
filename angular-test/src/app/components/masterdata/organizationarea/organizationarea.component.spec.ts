import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationAreaComponent } from './organizationarea.component';

describe('PerioxesforeonComponent', () => {
  let component: OrganizationAreaComponent;
  let fixture: ComponentFixture<OrganizationAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
