import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';


//service
import {HttpService} from '../../../services/HttpService';

//model
import {URLMASTERDATA} from '../../../model/Url';
import {PersonnelGroup} from '../../../model/masterdata/common/PersonnelGroup';
import {PersonnelCategory} from '../../../model/masterdata/common/PersonnelCategory';

@Component({
  selector: 'app-personnel-cat',
  templateUrl: './personnel-cat.component.html',
  styleUrls: ['./personnel-cat.component.css']
})
export class PersonnelCatComponent implements OnInit {

  personnelGroups:PersonnelGroup[] = [];
  personnelCategories:PersonnelCategory[] = [];


  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getAsync<PersonnelGroup>(URLMASTERDATA['personnelgroups']).subscribe(
      (res)=> {
        this.personnelGroups = res;
      },
      (err) => {
        console.log(err);
      }
    );
  }


  onRowSelectGroup(event) {
     this.httpService.getAsync<PersonnelCategory>(URLMASTERDATA['personnelcategories']).subscribe(
       (res)=>{
          if(res){
            this.personnelCategories = [];
            for(let i =0; i < res.length; i++){
              if(res[i].groupid){
                if(res[i].groupid == event.data.id){
                  this.personnelCategories.push(res[i]);
                }
              }
            }
          }
       },
       (err)=> {
         console.log(err);
       }
     ); 
  }

  onRowSelectCat(event) {

  }


}
