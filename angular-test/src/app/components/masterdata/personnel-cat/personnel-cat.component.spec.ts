import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnelCatComponent } from './personnel-cat.component';

describe('PersonnelCatComponent', () => {
  let component: PersonnelCatComponent;
  let fixture: ComponentFixture<PersonnelCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonnelCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnelCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
