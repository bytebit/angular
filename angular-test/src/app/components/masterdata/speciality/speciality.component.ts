import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//model import
import {Sector} from '../../../model/masterdata/common/Sector';
import {URLMASTERDATA} from '../../../model/Url';

//service import
import {HttpService} from '../../../services/HttpService';

@Component({
  selector: 'app-kladoi',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.css']
})

export class SpecialityComponent implements OnInit {

  sectors:Sector[];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<Sector>(URLMASTERDATA['sectors']).subscribe(
          (res)=> {
              //do more cheking

              //todo adapter/bridge class for httpservice
              this.sectors = res;
          },
          (err)=> {
            console.log(err);
          }
      );
  }

}
