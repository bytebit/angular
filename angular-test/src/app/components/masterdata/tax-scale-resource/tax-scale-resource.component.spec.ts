import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxScaleResourceComponent } from './tax-scale-resource.component';

describe('TaxScaleResourceComponent', () => {
  let component: TaxScaleResourceComponent;
  let fixture: ComponentFixture<TaxScaleResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxScaleResourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxScaleResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
