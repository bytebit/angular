import { Component, OnInit } from '@angular/core';

import {DataTableModule} from 'primeng/primeng';

//service
import {HttpService} from '../../../services/HttpService';

import {URLMASTERDATA} from '../../../model/Url';
import {TaxScale} from '../../../model/masterdata/common/TaxScale';
import {TaxScaleValues} from '../../../model/masterdata/common/TaxScaleValues';

@Component({
  selector: 'app-tax-scale-resource',
  templateUrl: './tax-scale-resource.component.html',
  styleUrls: ['./tax-scale-resource.component.css']
})
export class TaxScaleResourceComponent implements OnInit {

  taxScales:TaxScale[] = [];
  taxScaleValues:TaxScaleValues[] = [];

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      this.httpService.getAsync<TaxScale>(URLMASTERDATA['taxscales']).subscribe(
        (res)=> {
            this.taxScales = res;
        },
        (err)=> {
          console.log(err);
        }
      );
  }


  onRowSelectTaxScale(event) {

  }


  
  onRowSelectTaxScaleValue(event) {

  }

}
