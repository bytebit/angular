import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import {DataTableModule} from 'primeng/primeng';

import {HttpService} from '../../../../services/HttpService';

import {URLMASTERDATA} from '../../../../model/Url';
import {TaxScale} from '../../../../model/masterdata/common/TaxScale';
import {TaxScaleValues} from '../../../../model/masterdata/common/TaxScaleValues';


@Component({
  selector: 'app-taxscale',
  templateUrl: './taxscale.component.html',
  styleUrls: ['./taxscale.component.css']
})
export class TaxscaleComponent implements OnInit {

  taxScaleValues:TaxScaleValues[] = [];
  taxId:number;

  constructor(private httpService: HttpService,private route: ActivatedRoute,private location: Location) { }

  ngOnInit() {
    this.route.params
    .switchMap((params: Params) => this.httpService.getAsync<TaxScaleValues>(URLMASTERDATA['taxscales']+(this.taxId = params['id'])+'/values'))
    .subscribe(
       (res)=>{
          this.taxScaleValues = res;
       },
       (err)=> {
         console.log(err);
         //handle error , inform user
       }
    );
  }



  onRowSelectTaxScale(event) {

  }

}
