import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxscaleComponent } from './taxscale.component';

describe('TaxscaleComponent', () => {
  let component: TaxscaleComponent;
  let fixture: ComponentFixture<TaxscaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxscaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxscaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
