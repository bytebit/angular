import { Component, OnInit } from '@angular/core';
import {TreeNode} from 'primeng/primeng';
import {DropdownModule,InputTextModule,PanelModule,SelectItem,ButtonModule,FieldsetModule} from 'primeng/primeng';

//service import
import {HttpService} from '../../../services/HttpService';

//models
import {URLMASTERDATA} from '../../../model/Url';
import {TreeNodeForeas} from '../../../model/masterdata/common/TreeNodeForeas';
import {OrganizationArea} from '../../../model/masterdata/common/OrganizationArea';
import {OrganizationZone} from '../../../model/masterdata/common/OrganizationZone';


@Component({
  selector: 'app-foreis',
  templateUrl: './foreis.component.html',
  styleUrls: ['./foreis.component.css']
})

export class ForeisComponent implements OnInit {

  nodes:TreeNodeForeas[]; 
  selectedNode: TreeNodeForeas = new TreeNodeForeas();
  usrcode:string = '';
  description:string = '';
  selectedArea:number =0;
  selectedZone:number =0;

  zone:OrganizationZone = new OrganizationZone();
  area:OrganizationArea = new OrganizationArea();

  zones:SelectItem[] = [];
  areas:SelectItem[] = [];
  
   
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getAsync<TreeNodeForeas>(URLMASTERDATA['foreas']).subscribe(
      (res)=> {
         this.selectedNode = res[0];

         for(let i =0;i < res.length; i++) {
           res[i].label = res[i].description;
           res[i].expandedIcon = 'fa-folder-open';
           res[i].collapsedIcon = 'fa-folder';
         } 
        this.nodes = res;
      },
      (err)=> {
        console.log(err);
      }
    );


    this.httpService.getAsync<OrganizationArea>(URLMASTERDATA['organizationareas']).subscribe(
      (res)=> {
          for(let i =0; i < res.length; i++) {
            this.areas.push({label:res[i].description,value:res[i]});
          }
      },
      (err)=> {

      }
    );

    this.httpService.getAsync<OrganizationZone>(URLMASTERDATA['organizationzones']).subscribe(
      (res)=> {
          for(let i =0; i < res.length; i++) {
            this.zones.push({label:res[i].description,value:res[i]});
          }
      },  
      (err)=> {

      }
    );

  }


  //temp method
  nodeSelect(event) {
    
    this.usrcode = event.node.usrcode;
    this.description = event.node.description;

      if(event.node.expanded) {
        //event.node.expanded = false;
      } else {
        
        this.httpService.getAsync<TreeNodeForeas>(URLMASTERDATA['foreas']+event.node.id+'/foreas/').subscribe(
            (res)=> {
                if(res) {
                  for(let i =0; i < res.length; i++) {
                      res[i].label = res[i].description;
                      res[i].expandedIcon = 'fa-folder-open';
                      res[i].collapsedIcon = 'fa-folder';
                  }

                   
                   this.getZone(event.node.zone);
                   this.getArea(event.node.area);

                  event.node.expanded = true;
                  event.node.children = res;                 
                }
            },
            (err)=> {
                console.log(err);
            }
        );
      }
      
  }


  getZone(zoneCode:number){
      if(zoneCode) {

          this.httpService.getAsyncSingle<OrganizationZone>(URLMASTERDATA['organizationzones']+zoneCode).subscribe(
            (res)=> {
                this.zone = res;
                console.log(res);
            },
            (err)=>{
              console.log(err);
            }
          );
      }
  }


  getArea(areaCode:number){
      if(areaCode) {
        this.httpService.getAsyncSingle<OrganizationArea>(URLMASTERDATA['organizationareas']+areaCode).subscribe(
            (res) => {
                this.area = res;
                console.log(res);
            },
            (err)=> {
              console.log(err);
            }
        );
      }
  }


  nodeUnselect(event) {
      //do stuff
  }

}
