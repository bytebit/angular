import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForeisComponent } from './foreis.component';

describe('ForeisComponent', () => {
  let component: ForeisComponent;
  let fixture: ComponentFixture<ForeisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForeisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
