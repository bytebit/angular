import { Component, OnInit,OnDestroy,EventEmitter, Input, Output } from '@angular/core';

import { ActivatedRoute, Params } from '@angular/router';
import { Location }  from '@angular/common';
import { Http } from '@angular/http/src/http';
import { MaterialModule } from '@angular/material';

//services
import {HttpService} from '../../../services/HttpService';
import {ActionService} from '../../../services/ActionService';


//models
import {Misthologio} from '../../../model/masterdata/common/Misthologio';
import {ActionType} from '../../../model/ui/ActionType';
import {IActions} from '../../../model/ui/IActions';
import { Bundle } from "app/model/ui/Bundle";


//url api endpoint
import {URLMASTERDATA} from '../../../model/Url';
import { DataTableModule, SharedModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { DataType } from "app/model/ui/DataType";
import { ButtonType } from "app/model/ui/ButtonType";


//table imports

@Component({
  moduleId: module.id,
  selector: 'app-misthologio',
  templateUrl: './misthologio.component.html',
  styleUrls: ['./misthologio.component.css']
})

export class MisthologioComponent implements OnInit, OnDestroy, IActions {
   


  
  misthologia: Misthologio[];
  size: number = 0;
  selectedMisthologio: Misthologio;
  displayModal:boolean = false;
  displayModalNewMisthologio:boolean = false;


  constructor(private actionService: ActionService,private route: ActivatedRoute,private location: Location,private httpService: HttpService,private http: Http) { 
  }


  ngOnInit() {    
    this.actionService.register(this.constructor.name,this);

    
    this.httpService.getAsync<Misthologio>(URLMASTERDATA['misthologio']).subscribe(
      (res) => {
        for(let i =0; i < res.length; i++){
          res[i].postsLink = '/payroll/api/masterdata/misthologio/'+res[i].id+'/posts/';
        }
        this.misthologia = res;
        this.size = res.length;
      },
      (error)=> {
        console.log(error);
      }
    );   
    

    this.initButtons();
  }

  ngOnDestroy(): void {
      this.actionService.unRegister(this.constructor.name);
  }

  initButtons() {
    this.actionService.notifyButtonUi('MainContentComponent',ButtonType.CREATE,true)
      .notifyButtonUi('MainContentComponent',ButtonType.DELETE,false)
      .notifyButtonUi('MainContentComponent',ButtonType.INSERT,true)
      .notifyButtonUi('MainContentComponent',ButtonType.UPDATE,false);
  }


  showPosts(misthologio: Misthologio){
    console.log(misthologio);
  }

  editMisthologio(misthologio: Misthologio) {
      this.selectedMisthologio = misthologio;
      this.displayModal = true;
  }

  updatePost() {  
  }

  createMisthologio(usercode:string,desc:string){
    let newMis = new Misthologio();
    newMis.description = desc;
    newMis.usrcode = usercode;
  

    this.httpService.postAsync<Misthologio>(URLMASTERDATA['misthologio'],newMis).subscribe(
        (res)=> {
          console.log(res);
          //this.ngOnInit();
          this.misthologia = this.misthologia;
          this.misthologia.push(res);
        },
        (err)=> {
          console.log(err);
        }
    );
    this.displayModalNewMisthologio = false;
}


  deleteMisthologio() {
    //check todo before delete
  }


  //--start interface methods
  uiAction(button: ButtonType, state: boolean) {
  }

  insert() {
      this.displayModalNewMisthologio = true;
  }

  create() {
  }

  delete() {
  }

  update() {
  }

  receive(bundle: Bundle) {
  }
  //end interface methods


}
