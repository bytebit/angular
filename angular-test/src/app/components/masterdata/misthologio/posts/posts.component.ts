import { Component, OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

import 'rxjs/add/operator/switchMap';


//service import
import {HttpService} from '../../../../services/HttpService';
import {ActionService} from '../../../../services/ActionService';


//model import
import {Misthologio} from '../../../../model/masterdata/common/Misthologio';
import {Posts} from '../../../../model/masterdata/common/Posts';
import {URLMASTERDATA} from '../../../../model/Url';
import {ActionType} from '../../../../model/ui/ActionType';
import {IActions} from '../../../../model/ui/IActions';
import { Bundle } from "app/model/ui/Bundle";


//primeng import
import { DataTableModule, SharedModule, ButtonModule, DialogModule, InputTextModule } from 'primeng/primeng';
<<<<<<< HEAD
import { Button } from "app/model/ui/Button";
import { DataType } from "app/model/ui/DataType";
=======
import { DataType } from "app/model/ui/DataType";
import { ButtonType } from "app/model/ui/ButtonType";
>>>>>>> 7b0bd4c76489e8dcd1fd71eb219b5fd09fd21ddb

@Component({
  moduleId: module.id,
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit, OnDestroy, IActions {
   
  posts: Posts[];
  size: number;
  misthologioId:number;
  displayModal:boolean = false;
  displayModalNewPost:boolean = false;
  selectedPost:Posts;
  misthologio:Misthologio;

  constructor(private actionService:ActionService,private httpService: HttpService,private route: ActivatedRoute,private location: Location) {
   }


  ngOnInit() {
<<<<<<< HEAD
=======
    this.actionService.register(this.constructor.name,this);
>>>>>>> 7b0bd4c76489e8dcd1fd71eb219b5fd09fd21ddb
    this.initButtons();

    this.route.params
    .switchMap((params: Params) => this.httpService.getAsync<Posts>(URLMASTERDATA['misthologio']+(this.misthologioId = params['id'])+'/posts/'))
    .subscribe(
       (res)=>{

          this.posts = res;
          this.size = res.length;
          console.log(res);

          this.httpService.getAsyncSingle<Misthologio>(URLMASTERDATA['misthologio']+this.misthologioId).subscribe(
            (res) => {
                this.misthologio = res;
            },
            (err)=> {
              console.log(err);
            }
          );
       },
       (err)=> {
         console.log(err);
         //handle error , inform user
       }
    );
  }

  ngOnDestroy(): void {
    this.actionService.unRegister(this.constructor.name);
  }


  initButtons() {
    this.actionService.notifyButtonUi('MainContentComponent',ButtonType.CREATE,false)
      .notifyButtonUi('MainContentComponent',ButtonType.DELETE,true)
      .notifyButtonUi('MainContentComponent',ButtonType.INSERT,false)
      .notifyButtonUi('MainContentComponent',ButtonType.UPDATE,false);
  }

   initButtons() {
     let insertBtn = new Button();
     insertBtn.state = false;
     insertBtn.type = ActionType.INSERT;

     let bundle = new Bundle();
     bundle.data = insertBtn;
     bundle.type = DataType.ACTIONBUTTON;

     this.actionService.sendBundle(ActionType.SEND,bundle);

     let btn = new Button();
     btn.state = true;
     btn.type = ActionType.UPDATE;

     bundle = new Bundle();
     bundle.data = btn;
     bundle.type = DataType.ACTIONBUTTON;

     this.actionService.sendBundle(ActionType.SEND,bundle);
  }


  editPost(post:Posts) {
      this.selectedPost = post;
      this.displayModal = true;
  }


  updatePost() {
    this.displayModal = false;
    //check fields

    //do update
  }


  deletePost() {
    //confirm

    //delete
  }


  uiAction(button: ButtonType, state: boolean) {
  }


  insert() {
      this.displayModalNewPost = true;
  }

  create() {
  }

  delete() {
  }

  update() {
  }


  receive(bundle: Bundle) {
  }


}
