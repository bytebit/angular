import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisthologioComponent } from './misthologio.component';

describe('MisthologioComponent', () => {
  let component: MisthologioComponent;
  let fixture: ComponentFixture<MisthologioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisthologioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisthologioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
