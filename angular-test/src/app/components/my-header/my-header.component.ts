import { Component, OnInit,Output,EventEmitter } from '@angular/core';


@Component({
  selector: 'app-my-header',
  templateUrl: './my-header.component.html',
  styleUrls: ['./my-header.component.css']
})


export class MyHeaderComponent implements OnInit {

  accountDisplay:string = 'none';
  accountStatus:boolean = false;

  actionDisplay:string = 'none';
  actionStatus:boolean = false;

  @Output() navdrawer = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  add(n1: number,n2: number): number{
    return n1 + n2;
  }

  navClick(){
    this.navdrawer.emit(null);
  }

  accountClick() {
      this.accountStatus = !this.accountStatus;

      if(this.accountStatus){
        this.accountDisplay = 'table';
      } else {
        this.accountDisplay = 'none';
      }
  }

  actionClick() {
      this.actionStatus = !this.actionStatus;

      if(this.actionStatus){
        this.actionDisplay = 'table';
      } else {
        this.actionDisplay = 'none';
      }
  }

}
