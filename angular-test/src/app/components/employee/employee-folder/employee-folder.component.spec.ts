import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeFolderComponent } from './employee-folder.component';

describe('EmployeeFolderComponent', () => {
  let component: EmployeeFolderComponent;
  let fixture: ComponentFixture<EmployeeFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeFolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
