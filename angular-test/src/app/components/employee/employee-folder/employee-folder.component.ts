import { Component, OnInit} from '@angular/core';
import {RouterModule,ActivatedRoute,Router,Params} from '@angular/router';
import { Location }  from '@angular/common';
import { Http } from '@angular/http/src/http';

import {TabViewModule,FieldsetModule} from 'primeng/primeng';

import {HttpService} from '../../../services/HttpService';
import {EmployeeService} from '../../../services/EmployeeService';

import {URLEMPLOYEE} from '../../../model/Url';
import {Employee} from '../../../model/employee/Employee';


@Component({
  selector: 'app-employee-folder',
  templateUrl: './employee-folder.component.html',
  styleUrls: ['./employee-folder.component.css']
})

export class EmployeeFolderComponent implements OnInit {

  tabContent:string = '';

  constructor(private httpService: HttpService,private route: ActivatedRoute,private routenav: Router,private employeeService:EmployeeService) { }

  ngOnInit() {
    if(this.routenav.url == '/employee') {
        this.httpService.getAsync<Employee>(URLEMPLOYEE['employee']+'0/0').subscribe(
            (res) => {
                this.employeeService.employee = res[0];
                this.routenav.navigate(['/employee/record/'+this.employeeService.employee.id]);
            },
            (err)=> {

            }
      );
    }
     
  }



  onTabClick(event) {
    this.tabContent = 'Selected Tab:'+event.index;
    if(event.index == 0) {
       this.routenav.navigate(['/employee/record/'+this.employeeService.employee.id]);
    } else if(event.index == 2) {

    }
  }


}
