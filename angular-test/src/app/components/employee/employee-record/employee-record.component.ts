import { Component, OnInit } from '@angular/core';
import {RouterModule,ActivatedRoute,Router,Params} from '@angular/router';
import { Location }  from '@angular/common';
import { Http } from '@angular/http/src/http';

import {FieldsetModule,PanelModule} from 'primeng/primeng';

import {HttpService} from '../../../services/HttpService';
import {EmployeeService} from '../../../services/EmployeeService';

import {URLEMPLOYEE} from '../../../model/Url';
import {Employee} from '../../../model/employee/Employee';


@Component({
  selector: 'app-employee-record',
  templateUrl: './employee-record.component.html',
  styleUrls: ['./employee-record.component.css']
})
export class EmployeeRecordComponent implements OnInit {

  employee:Employee = new Employee();

  constructor(private httpService:HttpService,private route: ActivatedRoute,private employeeService: EmployeeService) { }

  ngOnInit() {
    
      this.route.params.subscribe(params => {
          this.httpService.getAsyncSingle<Employee>(URLEMPLOYEE['employee']+params['id']).subscribe(
            (res)=> {
                this.employeeService.employee = res;
            },
            (err)=> {

            }
          );
      });
  }



}
