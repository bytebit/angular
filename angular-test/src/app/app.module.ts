//@angular imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,JsonpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//lib imports
import {TreeModule as Tree} from 'angular2-tree-component';

//main components
import { AppComponent } from './app.component';
import { MyHeaderComponent } from './components/my-header/my-header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainContentComponent } from './components/main-content/main-content.component';


//main routes module
import {RouteMasterdata} from './routemodules/routemasterdata.module';
import {RouteemployeeModule} from './routemodules/routeemployee.module';



@NgModule({
  declarations: [
    AppComponent,
    MyHeaderComponent,
    FooterComponent,
    MainContentComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    RouteMasterdata,
    RouteemployeeModule,
    MaterialModule,
    BrowserModule,
    FormsModule,
    Tree,
  ],
  bootstrap: [AppComponent]
 
})


export class AppModule { }
