import { Component,OnInit,ViewChild  } from '@angular/core';
import {HttpService} from './services/HttpService';
import {MainContentComponent} from './components/main-content/main-content.component';
import {EmployeeService} from './services/EmployeeService';
import {ActionService} from './services/ActionService';


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[
  HttpService,
  EmployeeService,
  ActionService,
  ]
})


export class AppComponent implements OnInit  {
  

  constructor(){
  }

  ngOnInit(): void {
  }

}

