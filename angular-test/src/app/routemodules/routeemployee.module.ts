import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule,JsonpModule } from '@angular/http';

import { MaterialModule } from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {PanelModule,DataTableModule,TabViewModule,DropdownModule,SharedModule,ButtonModule,DialogModule,InputTextModule,TreeModule,TreeNode,DataListModule,ChartModule,FieldsetModule} from 'primeng/primeng';


//route imports
import {HomeComponent} from '../components/home/home.component';
import {EmployeeFolderComponent} from '../components/employee/employee-folder/employee-folder.component';
import {EmployeeRecordComponent} from '../components/employee/employee-record/employee-record.component';

const routes: Routes = [
  {path:'employee',component:EmployeeFolderComponent,children:[
    {path:'record/:id',component:EmployeeRecordComponent}
  ]},
];


@NgModule({
  imports: [
    MaterialModule,
    RouterModule.forChild(routes),
    NgbModule.forRoot(),
    PanelModule,
    DataTableModule,
    DropdownModule,
    SharedModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    TreeModule,
    DataListModule,
    ChartModule,
    FieldsetModule,
    TabViewModule
  ],
  declarations: [
    EmployeeFolderComponent,
    EmployeeRecordComponent
  ],
  exports:[
    RouterModule,
    EmployeeFolderComponent,
    EmployeeRecordComponent
  ]
})

export class RouteemployeeModule { }
