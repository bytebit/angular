import {NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule,JsonpModule } from '@angular/http';


import {TreeModule as Tree} from 'angular2-tree-component';


import { MaterialModule } from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//route components
import {HomeComponent} from '../components/home/home.component';
import {MisthologioComponent} from '../components/masterdata/misthologio/misthologio.component';
import {PostsComponent} from '../components/masterdata/misthologio/posts/posts.component';
import {OrganogramaComponent} from '../components/masterdata/organograma/organograma.component';
import {UnitComponent} from '../components/masterdata/organograma/unit/unit.component';
import {SectorComponent} from '../components/masterdata/sectors/sector.component';
import {SpecialityComponent} from '../components/masterdata/speciality/speciality.component';
import {ForeisComponent} from '../components/masterdata/foreis/foreis.component';
import {OrganizationAreaComponent} from '../components/masterdata/organizationarea/organizationarea.component';
import {TaxOfficeComponent} from '../components/masterdata/taxoffice/taxoffice.component';
import {PersonnelCatComponent} from '../components/masterdata/personnel-cat/personnel-cat.component';
import {TaxScaleResourceComponent} from '../components/masterdata/tax-scale-resource/tax-scale-resource.component';
import {TaxscaleComponent} from '../components/masterdata/tax-scale-resource/taxscale/taxscale.component';
import {EmploymentContractComponent} from '../components/masterdata/employment-contract/employment-contract.component';
import {AllowanceTypeComponent} from '../components/masterdata/allowance-type/allowance-type.component';
import {DeductionGroupComponent} from '../components/masterdata/deduction-group/deduction-group.component';

import {PanelModule,DataTableModule,DropdownModule,SharedModule,ButtonModule,DialogModule,InputTextModule,TreeModule,TreeNode,DataListModule,ChartModule,FieldsetModule} from 'primeng/primeng';


//angular2 frontend's url api url's
const routes: Routes = [
  {path:'',redirectTo:'/home',pathMatch:'full'},
  {path:'home',component:HomeComponent},
  {path:'misthologio',component:MisthologioComponent},
  {path:'misthologio/:id/posts',component:PostsComponent},
  {path:'unitroot',component:OrganogramaComponent,children:[
    {path:'unit/:id',component:UnitComponent}
  ]},
  {path:'sectors',component:SectorComponent},
  {path:'specialities',component:SpecialityComponent},
  {path:'foreis',component:ForeisComponent},
  {path:'organizationareas',component:OrganizationAreaComponent},
  {path:'taxoffices',component:TaxOfficeComponent},
  {path:'personnelcategories',component:PersonnelCatComponent},
  {path:'taxscale',component:TaxScaleResourceComponent},
  {path:'taxscale/:id',component:TaxscaleComponent},
  {path:'employmentcontracts',component:EmploymentContractComponent},
  {path:'allowancetypes',component:AllowanceTypeComponent},
  {path:'deductiongroups',component:DeductionGroupComponent},
];


@NgModule({
  declarations:[TaxOfficeComponent,
      MisthologioComponent,
      PostsComponent,
      OrganogramaComponent,
      UnitComponent,
      SectorComponent,
      SpecialityComponent,
      ForeisComponent,
      OrganizationAreaComponent,
      PersonnelCatComponent,
      TaxscaleComponent,
      TaxScaleResourceComponent,
      EmploymentContractComponent,
      HomeComponent,
      AllowanceTypeComponent,
      DeductionGroupComponent
  ],
  imports: [RouterModule.forRoot(routes),
  PanelModule,
    NgbModule.forRoot(),
    MaterialModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    TreeModule,
    SharedModule,
    ButtonModule,
    DialogModule,
    DataTableModule,
    InputTextModule,
    DataListModule,
    DropdownModule,
    ChartModule,
    FieldsetModule,
    ],
  exports: [RouterModule,
      HomeComponent,
      TaxOfficeComponent,
      MisthologioComponent,
      PostsComponent,
      OrganogramaComponent,
      UnitComponent,
      SectorComponent,
      SpecialityComponent,
      ForeisComponent,
      OrganizationAreaComponent,
      PersonnelCatComponent,
      TaxScaleResourceComponent,
      TaxscaleComponent,
      EmploymentContractComponent,
      AllowanceTypeComponent,
      DeductionGroupComponent
  ]
})


export class RouteMasterdata {

}
