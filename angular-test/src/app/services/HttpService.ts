import { Injectable }    from '@angular/core';
import { Headers, Http,Response,RequestOptions,URLSearchParams,Jsonp } from '@angular/http';

import 'rxjs/add/operator/map';
import { Observable }     from 'rxjs/Observable';

import {Entity} from '../model/Entity';

@Injectable()
export class HttpService {


  headers: Headers;
  options: RequestOptions;

  constructor(private http: Http){
    this.headers = new Headers({'Content-Type':'application/json'});
    this.options = new RequestOptions({headers: this.headers});
  }


  /**
   * getneric HTTP get method
   * returns array of the generic type 
   */
  getAsync<T extends Entity>(url: string):Observable<T[]> {
    return this.http
    .get(url,this.options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  
  /**
   * 
   * @param url the end point url for post
   * @param ob the object to create
   */
  postAsync<T extends Entity>(url:string,ob:T):Observable<T> {
    return this.http
     .post(url,JSON.stringify(ob),this.options)
     .map(this.extractData)
     .catch(this.handleError);
  }

  /**
   * 
   * @param url 
   * @param ob the object to update
   */
  putAsync<T extends Entity>(url:string,ob:T):Observable<T> {
    return this.http
     .put(url,JSON.stringify(ob),this.options)
     .map(this.extractData)
     .catch(this.handleError);
  }

  /**
   * 
   * @param url 
   * @param ob the object to update (partial update)
   */
  patchAsync<T extends Entity>(url:string,ob:T):Observable<T> {
    return this.http
     .patch(url,JSON.stringify(ob),this.options)
     .map(this.extractData)
     .catch(this.handleError);
  }

  /**
   * 
   * @param url 
   * returns a single object
   */
  getAsyncSingle<T extends Entity>(url: string):Observable<T> {
      return this.http
      .get(url,this.options)
      .map(this.extractData)
      .catch(this.handleError);
  }
 

 
  private extractData(res: Response){
      let body = res.json();
      return body || {};
  }


  //change to remote log infrastructure in future
  private handleError(error: Response | any){
      let errMsg: string;

      if(error instanceof Response) {
        const body  = error.json() || ' ';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {

      }

      console.log(errMsg);
      return Observable.throw(errMsg);
  }

}
