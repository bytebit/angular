import { Injectable }    from '@angular/core';
import {IActions} from '../model/ui/IActions';
import {ActionType} from '../model/ui/ActionType';
import { Bundle } from '../model/ui/Bundle';
import { ButtonType } from "app/model/ui/ButtonType";

@Injectable()
export class ActionService {

    observers:{[key:string]:IActions} = {};

    register(key:string,acts:IActions){
        this.observers[key] = acts;
    }

    unRegister(key:string) {
        delete this.observers[key];
    }

    /**
     * 
     * @param key 
     * @param type 
     */
    notify(key:string,type:ActionType){
        for(let k in this.observers) {
            if(k == key){
                let action = this.observers[key];
                this.doAction(type,action,null);
                break;
            }
        }    
    }

    /**
     * 
     * @param type 
     */
    notifyAll(type:ActionType){
        for(let key in this.observers) {
            let action =  this.observers[key];
            this.doAction(type,action,null);              
        }
    }


    /**
     * 
     * @param key 
     * @param button 
     * @param state 
     */
    notifyButtonUiAll(button:ButtonType,state:boolean){
        for(let k in this.observers){
            let action = this.observers[k];
            action.uiAction(button,state);            
        }
        return this;
    }


    /**
     * 
     * @param key 
     * @param button 
     * @param state 
     */
    notifyButtonUi(key:string,button:ButtonType,state:boolean){
        for(let k in this.observers){
            if(k == key) {
                let action = this.observers[k];
                action.uiAction(button,state);
                break;          
            }              
        }
        return this;
    }


    /**
     * 
     * @param type 
     * @param data 
     */
    sendBundle(type:ActionType,bundle:Bundle){
        for(let key in this.observers) {
            let action =  this.observers[key];
            this.doAction(type,action,bundle);              
        }
    }
    
    /**
     * 
     * @param type 
     * @param action 
     */
    private doAction(type:ActionType,action:IActions,data:any){
        if(type == ActionType.CREATE){
            action.create();
        } else if(type == ActionType.DELETE) {
            action.delete();
        } else if(type == ActionType.INSERT) {
            action.insert();
        } else if(type == ActionType.UPDATE) {
            action.update();
        } else if(type == ActionType.SEND) {
            action.receive(data);
        } 
    }


}