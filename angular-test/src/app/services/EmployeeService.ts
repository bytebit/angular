import { Injectable }    from '@angular/core';
import {Employee} from '../model/employee/Employee';

@Injectable()
export class EmployeeService {
    employee:Employee = new Employee();
}
