


export const URLMASTERDATA = {"misthologio":"http://localhost:8080/payroll/api/masterdata/misthologio/",
    "unit":"http://localhost:8080/payroll/api/masterdata/units/",
    "unitroot":"http://localhost:8080/payroll/api/masterdata/units?filter=root",
    "sectors":"http://localhost:8080/payroll/api/masterdata/sectors/",
    "foreas":"http://localhost:8080/payroll/api/masterdata/foreas/",
    "organizationzones":"http://localhost:8080/payroll/api/masterdata/organizationZones/",
    "organizationareas":"http://localhost:8080/payroll/api/masterdata/organizationAreas/",
    "taxoffices":"http://localhost:8080/payroll/api/masterdata/taxOffices/",
    "personnelcategories":"http://localhost:8080/payroll/api/masterdata/personnelCategories/",
    "personnelgroups":"http://localhost:8080/payroll/api/masterdata/personnelGroups/",
    "taxscales":"http://localhost:8080/payroll/api/masterdata/taxScales/",
    "employmentcontracts":"http://localhost:8080/payroll/api/masterdata/employmentContracts/",
    "allowancetypes":"http://localhost:8080/payroll/api/masterdata/allowanceTypes/",
    "deductiongroups":"http://localhost:8080/payroll/api/masterdata/deductions/groups/",
};

export const URLEMPLOYEE = {
    "employee":"http://localhost:8080/payroll/api/employees/"
};