import {Entity} from '../../Entity';

export class InsuranceDeduction extends Entity {
    usrcode:string;
    description:string;
    validFrom:string;
    validTo:string;
    approximation:number;
    comments:string;
    kodPosou:number;
    nonTaxable:number;
    backpayTaxPercent:number;
    promotionOnly:number;
    splitFixedAmmount:number;
    employeePercent:number;
    employerPercent:number;
    countOnStrike:number;
    codgsis:string;
    codgsiserg:string;
    codgsispros:string;
    insurer:number;
    branch:number;
    
}