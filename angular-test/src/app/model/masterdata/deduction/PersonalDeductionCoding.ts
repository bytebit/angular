import {Entity} from '../../Entity';

export class PersonalDeductionCoding extends Entity {
    usrcode:string;
    description:string;
    kodPosou:number;
    flagsom:number;
    parent:number;
}