import {Entity} from '../../Entity';

export class DeductionFormulaAllowance extends Entity {
    validFrom:string;
    validTo:string;
    employeePercent:number;
    employerPercent:number;
    limit:number;
    employeePercentLimit:number;
    employerPercentLimit:number;
    allowance:number;
    formCode:number;
}