import {Entity} from '../../Entity';

export class Insurer extends Entity {
    usrcode:string;
    description:string;
    isTax:number;
    shortDescription:string;
}