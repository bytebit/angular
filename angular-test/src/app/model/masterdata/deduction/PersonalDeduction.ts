import {Entity} from '../../Entity';

export class PersonalDeduction extends Entity {
    usrcode:string;
    description:string;
    synperig:string;
}