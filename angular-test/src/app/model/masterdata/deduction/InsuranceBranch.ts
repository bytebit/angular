import {Entity} from '../../Entity';

export class InsuranceBranch extends Entity {
    usrcode:string;
    description:string;
    shortDescription:string;
}