import {Entity} from '../../Entity';

export class InsuranceDeductionAmmounts extends Entity {
    fixedAmmount:number;
    percent:number;
    maxSummary:number;
    minSummary:number;
    cleanUp:number;
    formCode:number;
    
}