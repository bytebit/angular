import {Entity} from '../../Entity';

export class InsuranceDeductionGroup extends Entity {
    usrcode:string;
    description:string;
}