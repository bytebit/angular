import {Entity} from '../../Entity';

export class PersonalDeductionDetail extends Entity {
    usrcode:string;
    description:string;
    synperig:string;
    flagAfair:number;
    codgsis:number;
    typos:number;
    parent:number;
}