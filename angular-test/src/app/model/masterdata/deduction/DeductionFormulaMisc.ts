import {Entity} from '../../Entity';

export class DeductionFormulaMisc extends Entity {
    caseId:string;
    validFrom:string;
    validTo:string;
    employeePercent:number;
    employerPercent:number;
    formCode:number;
}