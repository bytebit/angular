import {Entity} from '../../Entity';

export class DeductionFormulaCompensation extends Entity {
    validFrom:string;
    validTo:string;
    employeePercent:number;
    employerPercent:number;
    formCode:number;
    compensation:number;
}