import {Entity} from '../../Entity';
import {PayrollType} from '../common/PayrollType';


export class InsuranceDeductionValue extends Entity {
    parent:number;
    caseid:PayrollType;
}