import {Entity} from '../../Entity';

export class InsuranceDeductionGroupItem extends Entity {
    dateValidFrom:string;
    description:string;
    dateValidTo:string;
    dateYpol:string;
    parent:number;
    deduction:number;
    secondDeduction:number;
}