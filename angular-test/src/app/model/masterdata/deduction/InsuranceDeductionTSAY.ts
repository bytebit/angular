import {Entity} from '../../Entity';

export class InsuranceDeductionTSAY extends Entity {
    et5:number;
    ygper:number;
    tk12:number;
    st1:number;
    st2:number;
    st3:number;
    st4:number;
    st5:number;
    st6:number;
    st7:number;
    st8:number;
    st9:number;
    st10:number;
    st11:number;
    st12:number;
    st13:number;
    st14:number;
    st15:number;
    st16:number;
    st17:number;
    st18:number;
    st19:number;
    st20:number;
    st21:number;
    st22:number;
    st23:number;
    st24:number;
    st25:number;
    st26:number;
    st27:number;
    st28:number;

    primary:number;
}