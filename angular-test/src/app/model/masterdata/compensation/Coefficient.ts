import {Entity} from '../../Entity';

export class Coefficient extends Entity {
    synd1:number;
    synd2:number;
    synd3:number;
    synd4:number;
    flagAnalEfhm:number;
}