import {Entity} from '../../Entity';

export class CompensationFormula extends Entity {
    description:string;
    aaypol:number;
    synd1:number;
    synd2:number;
    hmererg:number;
    flagbash:number;
    compensation:number;
    monada:number;
    grid:number;
    coeffGrid:number;
}