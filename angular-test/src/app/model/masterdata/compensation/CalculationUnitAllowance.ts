import {Entity} from '../../Entity';

export class CalculationUnitAllowance extends Entity {
    unit:number;
    allowance:number;
}