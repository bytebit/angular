import {Entity} from '../../Entity';

export class CompensationGroup extends Entity {
    dateFrom:string;
    pososyn:number;
    monadaplaf:number;
    gridplaf:number;
    parent:number;
}