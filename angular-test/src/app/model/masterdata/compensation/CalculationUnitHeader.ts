import {Entity} from '../../Entity';

export class CalculationUnitHeader extends Entity {
    usrcode:string;
    description:string;
}