import {Entity} from '../../Entity';

export class CompensationGrid extends Entity {
    usrcode:string;
    description:string;
    krit1:number;
    krit2:number;
    krit3:number;
    
}