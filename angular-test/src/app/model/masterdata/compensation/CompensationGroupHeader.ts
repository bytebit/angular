import {Entity} from '../../Entity';

export class CompensationGroupHeader extends Entity {
    usrcode:string;
    description:string;
}