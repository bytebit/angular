import {Entity} from '../../Entity';

export class Compensation extends Entity {
    usrcode:string;
    description:string;
    eidosMisthLog:number;
    seq:number;
    codgsis:number;
    kathg1:number;
    kathg2:number;
    kathg3:number;
    kathg4:number;
    //seqOrio:number;
    xorisPlafon:number;
    category:number;
}