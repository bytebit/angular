import {Entity} from '../../Entity';

export class CompensationGridValue extends Entity {
    krit1:string;
    krit2:string;
    krit3:string;
    poso:number;
    grid:number;
}