import {Entity} from '../../Entity';

export class Options extends Entity {
    optionName:string;
    optionValue:string;
}