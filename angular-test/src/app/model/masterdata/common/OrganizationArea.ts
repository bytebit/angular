
import {Entity} from '../../Entity';

export class OrganizationArea extends Entity {
    description:string;
    usrcode:string;
}