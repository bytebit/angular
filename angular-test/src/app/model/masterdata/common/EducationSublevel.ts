import {Entity} from '../../Entity';

export class EducationSublevel extends Entity {
    usrcode:string;
    description:string;
    master:number;
}