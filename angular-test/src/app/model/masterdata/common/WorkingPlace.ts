import {Entity} from '../../Entity';

export class WorkingPlace extends Entity {
    usrcode:string;
    description:string;
    parent:number;
}