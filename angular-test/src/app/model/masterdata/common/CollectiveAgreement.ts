import {Entity} from '../../Entity';

export class CollectiveAgreement extends Entity {
    usrcode:string;
    description:string;
}