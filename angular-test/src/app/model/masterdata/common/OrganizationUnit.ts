import {Entity} from '../../Entity';

export class OrganizationUnit extends Entity {
    description:string;
    parentUnit:number;
    unitType:number;
    usedbymis:number;
    usrcode:string;
}