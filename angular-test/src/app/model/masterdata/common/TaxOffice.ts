import {Entity} from '../../Entity';

export class TaxOffice extends Entity {
    usrcode:string;
    description:string;
    odos:string = '';
    xnumber:string = '';
    zipCode:string = '';
    phoneNum:string = '';
    fax:string = '';
    ypeuthunos:string = '';
    isactive:number;
}