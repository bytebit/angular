
import {Entity} from '../../Entity';

export class EmploymentContract extends Entity {
    usrcode:string;
    description:string;
    flag:number;
    status:number;
    aaesynet:number;
}