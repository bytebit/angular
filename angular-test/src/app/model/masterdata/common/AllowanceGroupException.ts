import {Entity} from '../../Entity';
import {PayrollType} from './PayrollType';

export class AllowanceGroupException extends Entity {
    flag:number
    parent:number;
    allowance:number;
    caseid:PayrollType;
}