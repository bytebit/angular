export enum Sex {
    MALE,
    FEMALE,
    UNKNOWN
}