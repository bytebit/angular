import {Entity} from '../../Entity';

export class Region extends Entity {
    usrcode:string;
    description:string;
}