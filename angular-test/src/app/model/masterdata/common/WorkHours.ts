import {Entity} from '../../Entity';

export class WorkHours extends Entity {
    usrcode:string;
    description:string;
    xstart:string;
    xstop:string;
    xstartAux:string;
    xstopAux:string;
}