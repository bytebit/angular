import {Entity} from '../../Entity';

export class ZipCode extends Entity {
    odos:string;
    ar:string;
    polh:string;
    nomos:string;
    zip:number;
    polhK:number;
    lnk:number;
}