
import {Entity} from '../../Entity';

export class AllowanceType extends Entity {
    usrcode:string;
    description:string;
    ab:number;
}