
import {Entity} from '../../Entity';

export class Allowance extends Entity {
    usrcode:string;
    description:string;
    flag:number;
    noForos:number;
    nomos:string = '';
    synperig1:string = '';
    synperig2:string = '';
    flagEsynet:number;
}