import {Entity} from '../../Entity';

export class Status extends Entity {
    usrcode:string;
    description:string;
    energos:number;
    organ:number;
    prosthetes:number;
}