import {Entity} from '../../Entity';
import {PayrollType} from './PayrollType';


export class AllowanceGroupValue extends Entity {
    flag:number;
    parent:number;
    caseid:PayrollType;
}