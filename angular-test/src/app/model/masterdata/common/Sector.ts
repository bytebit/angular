
import {Entity} from '../../Entity';

export class Sector extends Entity {

    usrcode:string;
    description:string;
    isDoctor:number;
    
}