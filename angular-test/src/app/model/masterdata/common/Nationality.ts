import {Entity} from '../../Entity';

export class Nationality extends Entity {
    usrcode:string;
    description:string;
}