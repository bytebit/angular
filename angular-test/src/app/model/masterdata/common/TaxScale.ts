import {Entity} from '../../Entity';

export class TaxScale extends Entity {
    usrcode:string;
    description:string;
    dateFrom:string;
    dateTo:string;

    posEkpt:number;
    ekptPaid1:number;
    ekptPaid2:number;
    ekptPaid3:number;
    ekptPaid4:number;
    ekptPaidPano:number;
    paidia:number;
    posoekpt:number;
    orioekpt:number;
    posostoekpt:number;
    
}