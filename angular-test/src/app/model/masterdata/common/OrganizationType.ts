import {Entity} from '../../Entity';

export class OrganizationType extends Entity {
    usrcode:string;
    description:string;
}