import {Entity} from '../../Entity';

export class AllowanceGroup extends Entity {
    usrcode:string;
    description:string;
}