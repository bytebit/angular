import {Entity} from '../../Entity';

export class PersonnelCategory extends Entity {
    description:string;
    groupid:number;
    tsayEidKathg:string = '';
    tsayErgSxesh:string = '';
    tsayThesh2:string = '';
    usrcode:string;
    vfStatus:number;
    aaesynet:number;
    idikomis:number;
    personnelGroup:number;
}