
import {Entity} from '../../Entity';

export class Speciality extends Entity {
    usrcode:string;
    description:string;
    sector:number;
}