import {Entity} from '../../Entity';

export class Echelon extends Entity {
    usrcode:string;
    description:string;
    perigrafi:string = '';
    codgsis:string = '';
    katEkp:number;
    vathmosId:number;
    poso:number;
    ethYpir:number;
}