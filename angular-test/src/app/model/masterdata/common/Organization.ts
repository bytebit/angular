
import {Entity} from '../../Entity';

export class Organization extends Entity {
    usrcode:string;
    description:string;
    stegi:number;
    agoni:number;
    isdype:number;
    apo:number;
    ews:number;
    unit:number;
    area:number;
    zone:number;
    parent:number;
}