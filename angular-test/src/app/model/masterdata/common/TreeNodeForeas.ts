
import {Entity} from '../../Entity';
import {TreeNode} from 'primeng/primeng';

export class TreeNodeForeas extends Entity implements TreeNode {

    //table fields
    usrcode:string;
    description:string;
    stegi:number;
    agoni:number;
    isdype:number;
    apo:number;
    ews:number;
    unit:number;
    area:number;
    zone:number;

    //not transformrelation
    parentId:number;



     //TreeNode fields
    label: string;
    data: any;
    icon: any;
    expandedIcon: any;
    collapsedIcon: any;
    children: TreeNode[];
    leaf: boolean;
    expanded: boolean;
    type: string;
    parent: TreeNode;
    partialSelected: boolean;
    styleClass: string;

}