import {Entity} from '../../Entity';

export class Country extends Entity {
    usrcode:string;
    description:string;
}