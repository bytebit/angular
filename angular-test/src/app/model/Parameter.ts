
import { Pair } from './Pair';

export interface Parameter<T> {

    params: Array<Pair<T>>;
}

