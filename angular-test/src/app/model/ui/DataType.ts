
export enum DataType {
    JSON,
    STRING,
    JSONARRAY,
    BINARY,
    ARRAY,
    ACTIONBUTTON,
}