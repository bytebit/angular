
export enum ActionType {
    INSERT,
    CREATE,
    UPDATE,
    DELETE,
    NEW,
    ALL,
    SEND,
    UI,
}