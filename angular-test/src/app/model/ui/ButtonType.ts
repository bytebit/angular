
export enum ButtonType {
    INSERT,
    CREATE,
    UPDATE,
    DELETE,
    ALL,
}