
import {ActionType} from './ActionType';
import {Bundle} from './Bundle';
import {ButtonType} from './ButtonType';

export interface IActions {

    insert();

    create();

    delete();

    update();

    receive(bundle:Bundle);

    uiAction(button:ButtonType,state:boolean);
}