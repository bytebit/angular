
export class Pair<T> {

  first: string;
  second: T;

  constructor(first:string,second:T){
    this.first = first;
    this.second = second;
  }

}
