
import {Entity} from './Entity';
import {TreeNode} from 'primeng/primeng';


export class MyTreeNode extends Entity implements TreeNode {
    //table fields
    description:string;
    parentUnit:number;
    unitType:number;
    usedbmis:number;
    usrcode:string;
    

    //TreeNode fields
    label: string;
    data: any;
    icon: any;
    expandedIcon: any;
    collapsedIcon: any;
    children: TreeNode[];
    leaf: boolean;
    expanded: boolean;
    type: string;
    parent: TreeNode;
    partialSelected: boolean;
    styleClass: string;
}