/**
 * Created by avraam on 24/2/2017.
 */

//den xrisimopoiite pleon
export const NODES = [
  {
    id:1,
    url:'/diaxiriseis',
    name:'Διαχειρίσεις',
    isLeaf:false,
  },
  {
    id:2,
    url:'/epeksergasia',
    name:'Επεξεργασία',
    isLeaf:false
  },
  { 
    id:3,
    url:'/pinakes',
    name:'Πίνακες',
    isLeaf:false,
    children:[
      {
        id:31,
        url:'/neaepidomata',
        name:'Νέα Επιδόματα',
        isLeaf:false,
        children:[
          {
            id:311,
            url:'/epidomata',
            name:'Επιδόματα',
            isLeaf:true,
          },
          {
            id:312,
            url:'/pinakeskritirion',
            name:'Πίνακες Κριτηρίων',
            isLeaf:true,
          }
        ]
      },
      {
        id: 32,
        url:'/organograma',
        name: 'Οργανόγραμα',
        isLeaf:true,
      },
      {
        id: 33,
        url:'/misthologio',
        name: 'Μισθολόγιa',
        isLeaf:true,
      },
        {
          id:35,
          url:'/kladoi',
          name:'Κλάδοι',
          isLeaf:true,
        },
        {
          id:36,
          url:'/eidikotites',
          name:'Ειδικότητες',
          isLeaf:true
        },
        {
          id:37,
          url:'/katekpedeusis',
          name:'Κατήγ./Υποκατ. Εκπαίδευσης',
          isLeaf:true
        },
        {
          id:38,
          url:'/foreis',
          name:'Φορείς',
          isLeaf:true
        },
        {
          id:39,
          url:'/perioxesforeon',
          name:'Περιοχές Φορέων',
          isLeaf:true
        },
        {
          id:301,
          url:'/efories',
          name:'Εφορίες',
          isLeaf:true
        },
        {
          id:302,
          url:'/katigprosopikou',
          name:'Κατηγορίες Προσωπικού',
          isLeaf:true
        },
        {
          id:303,
          url:'/pinforoumisth',
          name:'πίνακας φόρου Μισθωτών',
          isLeaf:true
        },
        {
          id:304,
          url:'/simvaseisergasias',
          name:'Συμβάσεις Εργασίας',
          isLeaf:true
        },
        {
          id:305,
          url:'/eidiapodoxon',
          name:'Είδη αποδοχών',
          isLeaf:true
        },
        {
          id:306,
          url:'/epidomata',
          name:'Επιδόματα',
          isLeaf:false,
          children:[
            {
                id:3061,
                url:'/formaepidomaton',
                name:'Φόρμες υπολογισμού επιδομάτων',
                isLeaf:true,
            }
          ]
        },
        {
          id:307,
          url:'/prosthetesamives',
          name:'Πρόσθετες αμοιβές',
          isLeaf:false,
        },
         {
          id:308,
          url:'/kratisis',
          name:'Κρατήσεις',
          isLeaf:false,
        },
        {
          id:309,
          url:'/perikopes',
          name:'Περικοπές',
          isLeaf:false,
        },
        {
          id:3010,
          url:'/parametroimis',
          name:'Παράμετροι μισθοδοσίας',
          isLeaf:true,
        }
    ]
  },
  {
    id:4,
    url:'/kiniseis',
    name:'Κινήσεις',
    isLeaf:false
  },
  {
    id:5,
    url:'/anafores',
    name:'Αναφόρες',
    isLeaf:false
  },
  {
    id:6,
    url:'/ipiresies',
    name:'Υπηρεσίες',
    isLeaf:false
  },
  {
    id:7,
    url:'/ergalia',
    name:'Εργαλεία',
    isLeaf:false
  },
  {
    id:8,
    url:'/help',
    name:'Βοήθεια',
    isLeaf:false
  },
  {
    id:22,
    url:'/category',
    name: 'category',
    isLeaf:true,
  },
];




