import {Entity} from '../Entity';

export class EmployeeAllowance extends Entity {
    dateFrom:string;
    dateTo:string;
    poso:number;
    pososto:number;
    pyy:number;
    pmm:number;
    pdd:number;
    arApof:string;
    hmniaApof:string;
    oldposo:number;
    oxiktisi:number;
    saveposo:number;
    allowance:number;
    employee:number;
    sid:number;
    echelon:number;
    misthologio:number;
}