import {Entity} from '../Entity';

export class EmployeePersonalDeduction extends Entity {
    comments:string;
    sunPoso:number;
    dateFrom:number;
    dateTo:number;
    arxPlDoseon:number;
    ypolDoshs:number;
    posoDoshs:number;
    flagAfair:number;
    arLogarDaneioleipth:string;
    yearFrom:number;
    yearTo:number;
    synKrat:number;
    toc:string;
    tin:string;
    firstname:string;
    lastname:string;
    iban:string;
    employee:number;
    deduction:number;
    
}