import {Entity} from '../../Entity';
import {RecordType} from './RecordType';

export class EmployeeRecord1 extends Entity {
    dt:string;
    weight:number;
    mid:number;
    met:RecordType;
    flags:string;
    ypSxesi:number;
    omid:number;
    omidE:number;
    misthologioId:number;
    postId:number;
    thesiId:number;
    ekpaideusiId:number;
    ypokatId:number;
    kladosId:number;
    eidikotitaId:number;
    kladosYpir:number;
    eidikotitaYpir:number;
    groupId:number;
    categoryId:number;
    orarioId:number;
    ores:number;
    foreasPouAnikei:number;
    foreasPouErg:number;
    sid:number;
    symvashErgasias:number;
    monimos:number;
    xwros:number;
    fp1:number;
    fp2:number;
    idiotitaId:number;
    misthosId:number;

    employee:number;
}