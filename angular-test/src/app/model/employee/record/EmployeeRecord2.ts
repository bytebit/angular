import {Entity} from '../../Entity';
import {RecordType} from './RecordType';

export class EmployeeRecord2 extends Entity {
    dt:string;
    weight:number;
    mid:number;
    met:RecordType;
    flags:string;
    ores:number;
    monimos:number;
    eidikotitaYpir:number;
    omidE:number;
    groupId:number;
    ypokatId:number;
    omid:number;
    categoryId:number;
    eidikotitaId:number;
    employee:number;
    ypSxesi:number;
    sid:number;
    orarioId:number;
    thesiId:number;
    fp2:number;
    fp1:number;
    foreasPouAnikei:number;
    idiotitaId:number;
    kladosId:number;
    foreasPouErg:number;
    postId:number;
    ekpaideusiId:number;
    misthosId:number;
    xwros:number;
    misthologioId:number;
    symvashErgasias:number;
    kladosYpir:number;
}