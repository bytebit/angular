import {Entity} from '../Entity';

export class EmployeePersonalDeductionGap extends Entity {
    dateFrom:number;
    dateTo:number;
    yearFrom:number;
    yearTo:number;
    parent:number;
}