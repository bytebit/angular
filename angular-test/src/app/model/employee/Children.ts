import {Entity} from '../Entity';

export class Children extends Entity {
    aa:number;
    birthday:string;
    name:string;
    sexCode:string;
    ageEpidoma:number;
    etosEpidoma:number;
    flagEpidoma:number;
    xroniaFoit:number;
    dateEggr:string;
    dateEnd:string;
    examFoit:number;
    examEpidoma:string;
    flagForce:number;
    paush:string;
    parent:number;
}