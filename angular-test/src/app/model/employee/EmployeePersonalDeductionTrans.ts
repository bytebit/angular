import {Entity} from '../Entity';

export class EmployeePersonalDeductionTrans extends Entity {
    posoDoshs:number;
    dFrom:number;
    yFrom:number;
    parent:number;
}