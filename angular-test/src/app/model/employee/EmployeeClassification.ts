import {Entity} from '../Entity';

export class EmployeeClassification extends Entity {
    flagPa:number;
    dateValidFrom:string;
    dateValidTo:string;
    aforolog:number;
    tekna:number;
    plafonKartTakt:number;
    plafonProstheton:number;
    plafonSynolApod:number;
    plafonPoso:number;
    employee:number;
    allowanceGroup:number;
    insurer:number;
    formCode:number;
    newInsurerBranch:number;
    compensationGroupHeader:number;
    
}